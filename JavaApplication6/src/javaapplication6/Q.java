/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication6;

public class Q {
    String q, t, f1, f2, f3,id;
    int lv;
    

    public Q(String q, String t, String f1, String f2, String f3,String id,int lv){
        this.q = q;
        this.t = t;
        this.f1 = f1;
        this.f2 = f2;
        this.f3 = f3;
        this.id = id;
        this.lv = lv;
        
    }

    Q(String q, String t, String f1, String f2, String f3) {
        this.q = q;
        this.t = t;
        this.f1 = f1;
        this.f2 = f2;
        this.f3 = f3;
    }
    public String getQ() {
        return q;
    }

    public String getT() {
        return t;
    }

    public String getF1() {
        return f1;
    }

    public String getF2() {
        return f2;
    }

    public String getF3() {
        return f3;
    }
    public String getid() {
        return id;
    }
    public int getLv() {
        return lv;
    }
    
}
